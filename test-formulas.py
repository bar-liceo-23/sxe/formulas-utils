from numpy import ndarray

import formulas
import easyFormulas


def print_all(item):
    print(type(item))
    print(item)


if __name__ == "__main__":
    filename = "resources/nomina-test.xlsx"
    cellRef = "F54"
    sheet = "NOMINA"
    # sheet_ranges = wb['range names']
    # print(sheet_ranges['D18'].value)
    print("--- draft ----")

    xl_model = formulas.ExcelModel().loads(filename).finish()
    solucion = xl_model.calculate()
    # solucion é te tipo schedula.utils.sol.Solution

    # print(type(solucion))
    # print(f'Solución:\n{solucion}\n--- end Solución -----------\n')
    cel = solucion.get("'[nomina-test.xlsx]NOMINA'!F54")
    print_all(cel)
    print_all(cel.values)
    values = cel.values["'[nomina-test.xlsx]NOMINA'!F54"]
    for v in values:
        if isinstance(v, ndarray):
            print_all(v)
            print_all(v.item(v.size - 1))

    print("--- forma chula ---")
    nomina = easyFormulas.EasySheet(filename)
    print(f"Gasto empresa: {nomina.get_value_of_cell(sheet, cellRef)}")
