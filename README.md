# Formulas Utils

This project is related with Excel spreadsheet utils. Some libraries can reads data from Excel sheets but, sometimes we found some problems reading cell values, for instance when they have formulas.

With this project you can read the value of cells that contains formulas easyly.

# Dependencies

The project dependencies are the following:
- numpy BSD
- formulas (EUPL 1.1) (EUPL 1.1+)

# License

This project was released under the terms of EUPL 1.2 License. You can see the full text in the LICENSE file.

# Usage

You can see an example of use and the way that I've found how to reads the formula values in test-formulas.py script.

# Author

Brais Arias Rio
