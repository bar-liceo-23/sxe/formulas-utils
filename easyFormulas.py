# Copyright (C) 2024 Brais Arias Rio
# This program is free software you can redistribute it and/or modify
# it under the terms of the EUPL-1.2-or-later
# You can see the full text of licence in LICENSE file
from numpy import ndarray

import formulas
import os


class EasySheet:
    def __init__(self, path):
        self.path = path
        xl_model = formulas.ExcelModel().loads(path).finish()
        self.solution = xl_model.calculate()

    def get_value_of_cell(self, sheet, cell):
        cell_ref = f"'[{os.path.basename(self.path)}]{sheet.upper()}'!{cell.upper()}"
        values = self.solution.get(cell_ref).values[cell_ref]
        for v in values:
            if isinstance(v, ndarray):
                return v.item(v.size - 1)
        return None
